"use strict";

const array = [23, "37", "Andrey", Infinity, NaN, null, true, false, undefined];

function filterBy(array, type) {
  return array.filter((item) => typeof item !== type);
}
console.log(filterBy(array, "string"));
