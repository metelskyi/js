"use strict";

const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
];

const divRoot = document.createElement('div');
divRoot.id = "root";
document.body.append(divRoot);

const ul = document.createElement('ul');
divRoot.append(ul);

books.forEach((item, index) => {
  const li = document.createElement("li");

  try{
      if (!item.author) {
          throw new SyntaxError(`No autor ${index}`);
      } 
      if (!item.name) {
          throw new SyntaxError(`No name ${index}`);
      }
      if (!item.price) {
          throw new SyntaxError(`No price ${index}`);
      }
      
      li.innerHTML = `${index}) author:${item.author}, name:${item.name}, price:${item.price}`;
      ul.append(li);

  } catch(error){
      console.log(error.message);
  }
});
