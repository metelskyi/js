"use strict";

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }

const newArr = {...employee, age: 49, salary: 5000};

console.log(newArr);
