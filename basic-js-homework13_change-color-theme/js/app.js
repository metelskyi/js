const themeBtn = document.querySelector(".theme");
const container = document.querySelector(".container");
const menu = document.querySelector(".menu");
const footermenu = document.querySelector(".footer-menu");
const nav = document.querySelector(".nav");
const left = document.querySelector(".left-menu");

container.style.backgroundColor = localStorage.getItem("container");
menu.style.backgroundColor = localStorage.getItem("menu");
footermenu.style.backgroundColor = localStorage.getItem("footermenu");
nav.style.color = localStorage.getItem("nav");
left.style.backgroundColor = localStorage.getItem("left");

themeBtn.addEventListener("click", (e) => {
  if (localStorage.getItem("themeColor") === "white") {
    container.style.backgroundColor = "rgb(64, 64, 64)";
    menu.style.backgroundColor = "#eb6c6c";
    footermenu.style.backgroundColor = "#eb6c6c";
    nav.style.color = "#eb6c6c";
    left.style.backgroundColor = "#eb6c6c";
    localStorage.setItem("themeColor", "black");
    localStorage.setItem("container", "rgb(64, 64, 64)");
    localStorage.setItem("menu", "#eb6c6c");
    localStorage.setItem("footermenu", "#eb6c6c");
    localStorage.setItem("nav", "#eb6c6c");
    localStorage.setItem("left", "#eb6c6c");
  } else {
    container.style.backgroundColor = "#ffffff";
    menu.style.backgroundColor = "#35444f";
    footermenu.style.backgroundColor = "rgba(99, 105, 110, 0.48)";
    nav.style.color = "#000000";
    left.style.backgroundColor = "#ffffff";
    localStorage.setItem("themeColor", "white");
    localStorage.setItem("container", "#ffffff");
    localStorage.setItem("menu", "#35444f");
    localStorage.setItem("footermenu", "rgba(99, 105, 110, 0.48)");
    localStorage.setItem("nav", "#000000");
    localStorage.setItem("left", "#ffffff");
  }
});
