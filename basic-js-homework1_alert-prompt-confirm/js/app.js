"use strict";

let name;
let age;

do {
  name = prompt("Enter your name");
} while (name.trim() === "" || name === null);

do {
  age = prompt("Enter your age");
} while (age === null || age.trim() === "" || isNaN(+age));

if (age < 18) {
  alert("You are not allowed to visit this website");
} else if (age >= 18 && age <= 22) {
  let question = confirm("Are you sure you want to continue?");
  if (question) {
    alert("Welcome, " + name);
  } else {
    alert("You are not allowed to visit this website");
  }
} else if (age > 22) {
  alert("Welcome, " + name);
}
