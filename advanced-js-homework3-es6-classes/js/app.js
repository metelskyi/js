"use strict";

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set name(name) {
        this._name = name;
    }

    get name() {
        return this._name;
        return this._name;
        return this._name;
    }

    set age(age) {
        this._age = age;
    }

    get age() {
        return this._age;
    }

    set salary(salary) {
        this._salary = salary;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return this._salary;
    }

    set salary(salary) {
        this._salary = salary * 3;
    }
}

const human1 = new Programmer('Andrey', '26', '5000', ["HTML, PHP"]);
console.log(human1);

const human2 = new Programmer('Oleg', '25', '4000', ["JS", "HTML, PHP"]);
console.log(human2);

const human3 = new Programmer('Aleksei', '24', '3000', ["CSS, RUBY"]);
console.log(human3);