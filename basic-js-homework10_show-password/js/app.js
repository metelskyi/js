"use strict";

let spanError = document.createElement("span");
let blockError = document.querySelector(".error");
blockError.append(spanError);

let passwd = document.querySelector(".btn");

passwd.addEventListener("click", (e) => {
  let input = e.target.parentNode.querySelectorAll("input");
  if (input[0].value !== input[1].value) {
    spanError.innerHTML =
      "<font color=red>Нужно ввести одинаковые значения</font>";
  } else {
    alert("You are welcome");
  }
});

let icons = document.querySelectorAll(".icon-password");
icons.forEach(function (openhide) {
  openhide.addEventListener("click", (e) => {
    let input = e.target.parentNode.querySelector("input");
    if (input.getAttribute("type") == "password") {
      input.setAttribute("type", "text");
      e.target.classList.remove("fa-eye");
      e.target.classList.add("fa-eye-slash");
    } else {
      input.setAttribute("type", "password");
      e.target.classList.remove("fa-eye-slash");
      e.target.classList.add("fa-eye");
    }
  });
});
