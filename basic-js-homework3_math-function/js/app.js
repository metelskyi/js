"use strict";

let number1;
let number2;
let operator;

do {
  number1 = prompt("Enter number 1");
} while (number1 === "" || number1 === null || isNaN(+number1));

do {
  number2 = prompt("Enter number 2");
} while (number2 === "" || number2 === null || isNaN(+number2));

operator = prompt("Enter operation");

function calc(number1, number2, operator) {
  if (operator === "+") {
    return +number1 + +number2;
  } else if (operator === "-") {
    return +number1 - +number2;
  } else if (operator === "*") {
    return +number1 * +number2;
  } else if (operator === "/") {
    return +number1 / +number2;
  }
  throw new Error("Correct operator was not found");
}
let response = calc(number1, number2, operator);
console.log(response);
